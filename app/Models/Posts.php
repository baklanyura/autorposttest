<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;

    protected $fillable  =['title','description'];

    public function authors(){
        return $this->belongsToMany(Authors::class);
    }
}
