<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorsPosts extends Model
{
    use HasFactory;
    protected $table = 'authors_posts';
}
