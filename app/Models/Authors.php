<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Authors extends Model
{
    use HasFactory;
    protected $fillable  =['name','age'];
    public function posts(){
        return $this->belongsToMany(Posts::class);
    }
}
