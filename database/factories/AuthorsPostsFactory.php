<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorsPostsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'authors_id' =>$this->faker->numberBetween(1,5),
            'posts_id' =>$this->faker->numberBetween(1,10),
        ];
    }
}
