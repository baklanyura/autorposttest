<?php

namespace Database\Seeders;

use App\Models\Authors;
use App\Models\AuthorsPosts;
use App\Models\Posts;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//        Authors::factory(5)->create();
//        Posts::factory(10)->create();
        AuthorsPosts::factory(10)->create();

    }
}
